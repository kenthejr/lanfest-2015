import string
from twilio.rest import TwilioRestClient
from util import formatinternational
from urllib.parse import urlencode

# set Twilio credentials
ACCOUNT_SID = "AC3099c83b7e9f7b0797f79108466a2e18"
AUTH_TOKEN = "9cb36e40a3b4b49bcb740f9a4bf60f7e"

# create Twilio client
client = TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

# get input from user via shell
number = input("Please type in your international formatted phone number (no spaces or dashes): ")
name = input("Please type in your name: ")

# format user's phone number
formattednumber = formatinternational(number)

# show formated number just for fun
print(formattednumber)

# construct the XML
with open("./ivr.xml") as stream:
    text = string.Template(stream.read()).substitute(name=name)

# construct the url
url = "http://urlecho.appspot.com/echo?" + urlencode({
    "status": 200,
    "Content-Type": "application/xml",
    "body": text
})

# call user's phone
call = client.calls.create(
    to=number,
    from_="+14159663378",
    method="GET",
    url=url,
)

# check status
sid = call.sid
status = None
while True:
    call = client.calls.get(sid)
    if call.status != status:
        status = call.status
        print(call.status)

    if call.status == "completed":
        break
