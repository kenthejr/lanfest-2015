# define a function to format international numbers
def formatinternational(num):
    pre = "+1"
    num = num.replace("-","")
    num = num.replace("(","")
    num = num.replace(")","")
    num = num.replace(" ","")
    return (pre + num)
